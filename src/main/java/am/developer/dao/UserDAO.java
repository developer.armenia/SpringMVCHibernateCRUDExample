/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package am.developer.dao;

import am.developer.model.User;
import am.developer.util.MD5Util;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author haykh
 */
@Repository
@Transactional
public class UserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf) {
        this.sessionFactory = sf;
    }

    public User register(User user) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(user);
        return user;
    }

    public List<User> validateUser(User user) {
        Session session = this.sessionFactory.getCurrentSession();
        @SuppressWarnings("unchecked")
        //List<User> userList = session.createQuery("from User").list();
        Query q = session.createQuery("select u from User u where u.email=:email and u.password=:password");
            q.setParameter("email", user.getUsername());
            q.setParameter("password", MD5Util.generateMD5(user.getPassword()));
        return q.list();
    }
    /* @Autowired
    DataSource datasource;
    @Autowired
    JdbcTemplate jdbcTemplate;

    public void register(UserDTO user) {
        String sql = "insert into users values(?,?,?,?,?,?,?)";
        jdbcTemplate.update(sql, new Object[]{user.getUsername(), user.getPassword(), user.getFirstname(),
            user.getLastname(), user.getEmail(), user.getAddress(), user.getPhone()});
    }

    public UserDTO validateUser(LoginDTO login) {
        String sql = "select * from users where username='" + login.getUsername() + "' and password='" + login.getPassword()
                + "'";
        List<UserDTO> users = jdbcTemplate.query(sql, new UserMapper());
        return users.size() > 0 ? users.get(0) : null;
    }*/
}

 
