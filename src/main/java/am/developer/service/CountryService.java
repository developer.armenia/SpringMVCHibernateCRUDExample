package am.developer.service;

import java.util.List;
import am.developer.dao.CountryDAO;
import am.developer.model.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("countryService")
@Transactional
public class CountryService {

    @Autowired
    private CountryDAO countryDao;

    public List<Country> getAllCountries() {
        return countryDao.getAllCountries();
    }

    public Country getCountry(int id) {
        return countryDao.getCountry(id);
    }

    public void addCountry(Country country) {
        countryDao.addCountry(country);
    }

    public void updateCountry(Country country) {
        countryDao.updateCountry(country);

    }

    public void deleteCountry(int id) {
        countryDao.deleteCountry(id);
    }
}
